# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
#from boto.s3.connection import S3Connection
#from boto.sqs.connection import SQSConnection
#from boto.sqs.message import Message
#import storages.backends.s3boto

#!/usr/bin/env python
# -*- encoding:utf-8 -*-
from StringIO import StringIO
import uuid

from django.contrib.auth.models import AbstractUser


from django.views.decorators.csrf import csrf_exempt

import wioframework.fields as models

from wioframework import decorators as dec
from wioframework.amodels import wideiomodel, wideio_owned, wideio_timestamped_autoexpiring, get_dependent_models, wideio_timestamped, wideio_action

import settings

MODELS = []


@wideiomodel
@wideio_owned()
@wideio_timestamped
class File(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True, help_text="Name of the file, or name of input parameter for an algorithm")
    file = models.FileField(blank=True,
                            null=True,
                            help_text="path to a file",
                            upload_to=(lambda instance,
                                       filename: ((lambda s,
                                                   e: "upload/files/%s/%s.%s" % (s[:3],
                                                                                 s,
                                                                                 e))(str(uuid.uuid1()),
                                                                                     (filename.split(".")[-1])[-4:]))),
                            **({'storage': storages.backends.s3boto.S3BotoStorage(bucket=getattr(settings,"AWS_STORAGE_BUCKET_NAME","wideio"))} 
                               if getattr(settings,"AWS_READY",False) else {}))
    reference = models.ForeignKey('File',related_name="original_file",null=True)

    @staticmethod
    def can_add(request):
        # TODO: FIXME ADD RATE LIMIT TO THIS CALL TO AVOID SERVER SPAMMING
        return True

    def on_add(self, request):
        if request.user.id is not None:
            pass
        elif "token" in request.REQUEST:
            from compute.models import Token
            try:
                token = Token.objects.filter(
                    security_key=request.REQUEST["token"])[0]
            except:
                raise dec.PermissionException("Bad Token")
            self.owner = token.issued_by
            self.save()
        else:
            raise dec.PermissionException("Bad Token")

    def __unicode__(self):
        return "webdata/" + unicode(self.file)

    def on_delete(self, request):
        self.file.delete(False)

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only
        # permissions=dec.perm_777 # FIXME !!!
        form_exclude = ['reference']
        audlv_xargs = {'add_decorators': [csrf_exempt]}
        API_EXPORT_EXCLUDE = ["file"]
        MOCKS = {
            'default': [
                {
                    'name': 'file-0000'
                }
            ]
        }

MODELS.append(File)
MODELS += get_dependent_models(File)
