#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
"""
WIDE IO
Revision FILE: $Id: widgets.py 841 2009-08-23 13:01:52Z tranx $


#############################################################################################################
  copyright $Copyright$
  @version $Revision: 841 $
  @lastrevision $Date: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
  @modifiedby $LastChangedBy: tranx $
  @lastmodified $LastChangedDate: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
#############################################################################################################
"""
# create widgets related to jquery component for essential displays ...
import datetime
import re
import uuid

from django.db.models.fields import NOT_PROVIDED
from django.forms.widgets import Widget
from django.template import Context, loader
from django.utils.safestring import mark_safe


import  settings


def getuuid():
    return str(uuid.uuid1())


class AFileWidget(Widget):

    """
    For uploading AJAX-ready django files...
    """
    class Media:
        #css = {'all': ["css/jquery.wysiwyg.css"]}
        js = ["js/jquery-form.js"]

    def __init__(self, attrs={}):
        self.attrs = attrs
        self.uuid = getuuid()
        self.request = None

    def set_request(self, r):
        self.request = r

    def render(self, name, ovalue, attrs=None):
        from models import File
        # < create a temporary file object on which we work..
        # < if the file is modified or accepted we will change the value
        # < otherwise we should return the previous reference file
        if (type(ovalue) in [unicode, str]):
            ovalue = File.objects.get(id=ovalue)
        value = File()
        value.reference = ovalue
        value.save()
        if (ovalue is not None):
            value.file = ovalue.file  # TODO: CHECK IF THIS FAST OR SLOW
        value.on_add(self.request)
        value.save()
        widgettemplate = loader.get_template('extfields/afile.html')
        html_content = widgettemplate.render(Context(
            {'name': name, 'value': value, 'MEDIA_URL': settings.MEDIA_URL, 'uuid': self.uuid}))
        return html_content

    def get_prologue(self):
        return """
        <div style="visibility:hidden; height:1px; width: 1px; ">
            <form id="myForm%(uuid)s" action="%(update_url)s" method="POST" enctype="multipart/form-data" style="visibility:hidden;">
                <input type="hidden" name="name"  value="truc" />
                <input type="file" name="file" />
                <input type="submit" value="Upload File" />
            </form>
        </div>
        """ % ({"update_url": "not-yet-set", "uuid": self.uuid})
